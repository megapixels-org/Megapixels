#ifdef GL_ES
precision mediump float;
#endif

attribute vec2 vert;
attribute vec2 tex_coord;

uniform mat3 transform;
uniform vec2 pixel_size;

varying vec2 texture_coord;

void
main()
{
    texture_coord = vert.xy * vec2(0.5, 0.5) + vec2(0.5, 0.5);
    gl_Position = vec4(transform * vec3(vert, 1), 1);
}
