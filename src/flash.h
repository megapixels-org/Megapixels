#include "gio/gio.h"
#include <libmegapixels.h>

void mp_flash_gtk_init(GDBusConnection *conn);
void mp_flash_gtk_clean();

void mp_flash_enable(libmegapixels_camera *camera);
void mp_flash_disable(libmegapixels_camera *camera);
